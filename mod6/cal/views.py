# Create your views here.
import json
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib.auth import authenticate, login, logout
from django.views.generic.edit import FormView
from django.http import HttpResponse
from django.contrib import messages
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.forms import UserCreationForm
from random import randint
from decorators import require_AJAX
from forms import LoginForm, AddCalendarForm
from models import Cal

@require_AJAX
@require_http_methods(["POST"])
def login_submit(request):
	username = request.POST['login-username']
	password = request.POST['login-password']
	user = authenticate(username=username, password=password)
	if user is not None and user.is_active:
		login(request, user)
		messages.success(request, "You've been logged in")
		rendered_header = render_to_string("cal/user_header.html", {'user' : user}, context_instance=RequestContext(request))
		rendered_calendars = render_to_string("cal/calendars.html", {"form" : AddCalendarForm(), "calendars" : Cal.objects.filter(user=request.user.id)}, context_instance=RequestContext(request))
		rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
		return HttpResponse(json.dumps({"success" : "true", "messages_html" : rendered_messages, "header_html" : rendered_header, "calendars_html" : rendered_calendars}), content_type="application/json")
	else:
		messages.error(request, 'Invalid username or password')
		rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
		return HttpResponse(json.dumps({"success" : "false", "messages_html" : rendered_messages}), content_type="application/json")

@require_AJAX
@require_http_methods(["POST"])
def signup_submit(request):
	form = UserCreationForm(request.POST)
	if form.is_valid():
		user = form.save()
		messages.info(request, "Thanks for registering. You are now logged in.")
		user = authenticate(username=request.POST['signup-username'], password=request.POST['signup-password2'])
		if user is not None and user.is_active:
			login(request, user)
			rendered_header = render_to_string("cal/user_header.html", {'user': user}, context_instance=RequestContext(request))
			rendered_calendars = render_to_string("cal/calendars.html", {"form" : AddCalendarForm(), "calendars" : Cal.objects.filter(user=request.user.id)}, context_instance=RequestContext(request))
			rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
			return HttpResponse(json.dumps({"success" : "true", "messages_html" : rendered_messages, "header_html" : rendered_header, "calendars_html" : rendered_calendars}), content_type="application/json")
		else:
			messages.error(request, 'Failed to signup')
			rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
			return HttpResponse(json.dumps({"success" : "false", "messages_html" : rendered_messages}), content_type="application/json")
	else:
		messages.error(request, 'Failed to signup')
		rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
		return HttpResponse(json.dumps({"success" : "false", "messages_html" : rendered_messages}), content_type="application/json")

def logout_submit(request):
	logout(request)
	messages.success(request, 'You are successfuly logged out')
	rendered_header = render_to_string("cal/user_header.html", {"form" : LoginForm(prefix="login"), "signup_form" : UserCreationForm(prefix="signup")}, context_instance=RequestContext(request))
	rendered_calendars = render_to_string("cal/calendars.html", {"form" : AddCalendarForm(), "calendars" : Cal.objects.filter(user=request.user.id)}, context_instance=RequestContext(request))
	rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
	return HttpResponse(json.dumps({"success" : "true", "messages_html" : rendered_messages, "header_html" : rendered_header, "calendars_html" : rendered_calendars}), content_type="application/json")
	
@require_AJAX
@require_http_methods(["POST"])	
def add_calendar(request):
	user = request.user
	form = AddCalendarForm(request.POST)
	if form.is_valid() and user is not None:
		color = randint(1, 32)
		title = request.POST['title']
		description = request.POST['description']
		calendar = Cal.objects.create(user=user, title=title, description=description, color=color)
		messages.success(request, 'New calendar added!')
		rendered_calendars = render_to_string("cal/calendars.html", {"form" : AddCalendarForm(), "calendars" : Cal.objects.filter(user=request.user.id)}, context_instance=RequestContext(request))
		rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
		return HttpResponse(json.dumps({"success" : "true", "calendars_html" : rendered_calendars, "messages_html" : rendered_messages}), content_type="application/json")
	else:
		messages.error(request, 'Failed to add calendar')
		rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
		return HttpResponse(json.dumps({"success" : "false", "messages_html" : rendered_messages}), content_type="application/json")

@require_AJAX
@require_http_methods(["POST"])
def delete_calendar(request):
	calendar = Cal.objects.get(title=request.POST['calendar-title'])
	calendar.delete()
	messages.success(request, 'You successfuly deleted a calendar')
	rendered_calendars = render_to_string("cal/calendars.html", {"form" : AddCalendarForm(), "calendars" : Cal.objects.filter(user=request.user.id)}, context_instance=RequestContext(request))
	rendered_messages = render_to_string("cal/messages.html", {}, RequestContext(request))
	return HttpResponse(json.dumps({"success" : "true", "calendars_html" : rendered_calendars, "messages_html" : rendered_messages}), content_type="application/json")

def index(request):
	form = LoginForm(prefix="login")
	
	return render(request, "cal/index.html", {'user' : request.user, 'form': form, 'signup_form': UserCreationForm(prefix="signup"), "add_cal_form" : AddCalendarForm(), "calendars" : Cal.objects.filter(user=request.user.id)})
