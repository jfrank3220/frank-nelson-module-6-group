from django import forms
from django.contrib.auth.models import User

class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)
	
class AddCalendarForm(forms.Form):
	title = forms.CharField(label='Title', widget=forms.TextInput(attrs={'placeholder': 'Title'}))
	description = forms.CharField(label='Description', widget=forms.TextInput(attrs={'placeholder': 'Description'}))
