from django.conf.urls import patterns, url

from cal import views

urlpatterns = patterns('', 
	#url(r'^$', views.CalendarView.as_view(), name='index'),
	url(r'^$', views.index, name='index'),
	url(r'^login_submit/$', views.login_submit, name='login_submit'),
	url(r'^signup_submit/$', views.signup_submit, name='signup_submit'),
	url(r'^logout_submit/$', views.logout_submit, name='logout_submit'),
	url(r'^add_calendar/$', views.add_calendar, name='add_calendar'),
	url(r'^delete_calendar/$', views.delete_calendar, name='delete_calendar'),
)