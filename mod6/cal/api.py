from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie.paginator import Paginator
from cal.models import Cal, Event
from tastypie import fields
from tastypie.fields import ToOneField
from django.contrib.auth.models import User
from auth import UserObjectsOnlyAuthorization


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['email', 'password', 'is_active', 'is_staff', 'is_superuser']
        allowed_methods = ['get']

class CalResource(ModelResource):
    user = ToOneField(UserResource, "user", full=True)
    class Meta:
        queryset = Cal.objects.all()
        paginator_class = Paginator
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'user': ALL_WITH_RELATIONS,
        }

class EventResource(ModelResource):
    user = ToOneField(UserResource, "user", full=True)
    cid = ToOneField(CalResource, "cal", full=True)
    class Meta:
        queryset = Event.objects.all()
        paginator_class = Paginator
        always_return_data = True
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'user': ALL_WITH_RELATIONS,
        }
    def hydrate_cid(self, bundle):
        bundle.data["cid"] = "/cal/%d/" % bundle.data["cid"]
        return bundle
    def hydrate_user(self, bundle):
        bundle.data["user"] = "/user/%s/" % bundle.request.user.id
        return bundle
    def hydrate_id(self, bundle):
        if bundle.data["id"] == 0:
            bundle.data["id"] = None
        return bundle
    def dehydrate_id(self, bundle):
        return bundle.obj.id
    def dehydrate_cid(self, bundle):
        return bundle.obj.cal.id
