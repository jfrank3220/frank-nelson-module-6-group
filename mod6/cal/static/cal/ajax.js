function ajax_responses() {
    $('#form-login').submit(function () {
        $.ajax({
            type: "POST",
            url: "login_submit/",
            data: $('#form-login').serialize(),
            success: function (response) {
                if (response.success == "true") {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                    $('#messages').delay(1000).fadeOut();
                    $('#user_header').html(response.header_html);
                    $('#calendars').html(response.calendars_html);
                    Ext.getStore("Events").load();
                    Ext.getStore("Calendars").load();                    
                }
                else {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                }
                ajax_responses();
            }
        });
        return false;
    });

    $('#form-signup').submit(function() {
        $.ajax({
            type: "POST",
            url: "signup_submit/",
            data: $('#form-signup').serialize(),
            success: function (response) {
                if (response.success == "true") {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                    $('#messages').delay(1000).fadeOut();
                    $('#user_header').html(response.header_html);
                    $('#calendars').html(response.calendars_html);
                    Ext.getStore("Events").load();
                    Ext.getStore("Calendars").load();                    
                }
                else {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                }
                ajax_responses();
            }
        });
        return false;
    });

    $('#form-logout').submit(function () {
        $.ajax({
            type: "POST",
            url: "logout_submit/",
            data: $('#form-logout').serialize(),
            success: function (response) {
                if (response.success == "true") {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                    $('#messages').delay(1000).fadeOut();
                    $('#user_header').html(response.header_html);
                    $('#calendars').html(response.calendars_html);
                    Ext.getStore("Events").loadData([], false);
                    Ext.getStore("Events").load();
                    Ext.getStore("Calendars").loadData([], false);
                    Ext.getStore("Calendars").load();
                }
                else {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                }
                ajax_responses();
            }
        });
        return false;
    });

    $('#form-add-calendar').submit(function() {
        $.ajax({
            type: "POST",
            url: "add_calendar/",
            data: $('#form-add-calendar').serialize(),
            success: function (response) {
                if (response.success == "true") {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                    $('#messages').delay(1000).fadeOut();
                    $('#calendars').html(response.calendars_html);
                    Ext.getStore("Events").loadData([], false);
                    Ext.getStore("Events").load();
                    Ext.getStore("Calendars").loadData([], false);
                    Ext.getStore("Calendars").load();
                }
                else {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                }
                ajax_responses();
            }
        });
        return false;
    });

    $('#form-delete-calendar').submit(function() {
        $.ajax({
            type: "POST",
            url: "delete_calendar/",
            data: $('#form-delete-calendar').serialize(),
            success: function (response) {
                if (response.success == "true") {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                    $('#messages').delay(1000).fadeOut();
                    $('#calendars').html(response.calendars_html);
                    Ext.getStore("Events").loadData([], false);
                    Ext.getStore("Events").load();
                    Ext.getStore("Calendars").loadData([], false);
                    Ext.getStore("Calendars").load();
                }
                else {
                    $('#messages').fadeIn();
                    $('#messages').html(response.messages_html);
                }
                ajax_responses();
            }
        });
        return false;
    });


}

$(document).ready(function(){
    ajax_responses();
});