from django.conf.urls import patterns, include, url
from cal.api import CalResource, EventResource, UserResource

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

cal_resource = CalResource()
event_resource = EventResource()
user_resource = UserResource()

urlpatterns = patterns('',

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('cal.urls')),
    url(r'^', include(cal_resource.urls)),
    url(r'^', include(event_resource.urls)),
    url(r'^', include(user_resource.urls)),
)
